import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

public class DeleteFile {
    public static final String fileName = "file.txt";

    public static void DeleteFile() {
        File file = new File(fileName);
        file.delete();

    }

    public static void deleteOneLine (String lineContent) throws IOException
    {
        File file = new File("file.txt");
        List<String> out = Files.lines(file.toPath())
                .filter(line -> !line.contains(lineContent))
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }

//    public static void DeleteLastLine(){
//        File file = new File(fileName);
//        String oldContent = "";
//
//        try {
//            FileReader fileReader = new FileReader(fileName);
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//            String line = null;
//
//            while ((line = bufferedReader.readLine()) != null) {
//                oldContent = oldContent + line + System.lineSeparator();
//            }
//
//            String newContent = line.trim();
//            FileWriter fileWriter = new FileWriter(fileName);
//            fileWriter.write(newContent);
//
//            bufferedReader.close();
//            fileWriter.close();
//        }catch (IOException e){
//            e.printStackTrace();
//        }
//    }
}

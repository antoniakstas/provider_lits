public class Provider {

    private String name;
    private String city;
    private String termin;
    private String dateOfPrice;

    public Provider(){

    }

    public Provider(String name, String city, String termin, String dateOfPrice){
        this.name = name;
        this.city = city;
        this.termin = termin;
        this.dateOfPrice = dateOfPrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setTermin(String termin) {
        this.termin = termin;
    }

    public void setDateOfPrice(String dateOfPrice) {
        this.dateOfPrice = dateOfPrice;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getTermin() {
        return termin;
    }

    public String getDateOfPrice() {
        return dateOfPrice;
    }

    public String toString(){
        return name+" "+city+" "+termin+" "+dateOfPrice;
    }
}

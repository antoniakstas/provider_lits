import java.io.*;

public class UpdateFile {
    public static void UpdateFile(String fileName, String oldString, String newString) {
        String oldContent = "";

        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                oldContent = oldContent + line + System.lineSeparator();
            }

            String newContent = oldContent.replaceAll(oldString, newString);
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(newContent);

            bufferedReader.close();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


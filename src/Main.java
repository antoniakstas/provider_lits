import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter 1 for Create and Write to file\n2 for Read all from file\n3 for Update file\n4 for Delete file\n5 for Delete one line: ");
        int a = scanner.nextInt();
            switch (a) {
                case 1:
                    WriteToFile.WriteToFile();
                    break;
                case 2:
                    ReadAllFromFile.ReadAllFromFile();
                    break;
                case 3:
                    System.out.println("enter the old line: ");
                    String oldLine = scanner.next();
                    System.out.println("enter the new line: ");
                    String newLine = scanner.next();
                    UpdateFile.UpdateFile("file.txt", oldLine, newLine);
                    break;
                case 4:
                    DeleteFile.DeleteFile();
                    break;
                case 5:
                    System.out.println("enter the line you want to delete: ");
                    String lineToDelete = scanner.next();
                    DeleteFile.deleteOneLine(lineToDelete);
                    break;
            }
            scanner.close();
    }
}

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ReadAllFromFile {
    public static final String fileName = "file.txt";
    public static void ReadAllFromFile () {

            List<Provider> list = new ArrayList<>();

            try {
                FileReader fr = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fr);
                String line = null;
                int currentLine = 0;


                while ((line = bufferedReader.readLine()) != null) {

                    char delimiter = '|';
                    int positionOfFirstDelimiter = line.indexOf(delimiter);
                    int positionOfSecondDelimiter = line.indexOf(delimiter, positionOfFirstDelimiter + 1);
                    int positionOfThirdDelimiter = line.indexOf(delimiter, positionOfSecondDelimiter + 1);
                    int positionOfFourthDelimiter = line.indexOf(delimiter, positionOfThirdDelimiter + 1);
                    int positionOfFifthDelimiter = line.indexOf(delimiter, positionOfFourthDelimiter + 1);

                    String name = getName(line, positionOfFirstDelimiter + 1, positionOfSecondDelimiter);
                    String city = getCity(line, positionOfSecondDelimiter + 1, positionOfThirdDelimiter);
                    String termin = getTermin(line, positionOfThirdDelimiter + 1, positionOfFourthDelimiter);
                    String dateOfPrice = getDateOfPrice(line, positionOfFourthDelimiter + 1, positionOfFifthDelimiter);

                    Provider p1 = new Provider();

                    p1.setName(name);
                    p1.setCity(city);
                    p1.setTermin(termin);
                    p1.setDateOfPrice(dateOfPrice);

                    list.add(p1);
                    System.out.println(p1.toString());

                }
                bufferedReader.close();


            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    private static String getName(String line, int positionOfFirstDelimiter, int positionOfSecondDelimiter){
        return line.substring(positionOfFirstDelimiter,positionOfSecondDelimiter);
    }

    private static String getCity(String line, int positionOfSecondDelimiter, int positionOfThirdDelimiter){
        return line.substring(positionOfSecondDelimiter,positionOfThirdDelimiter);
    }

    private static String getTermin(String line, int positionOfThirdDelimiter, int positionOfFourthdDelimiter){
        return line.substring(positionOfThirdDelimiter,positionOfFourthdDelimiter);
    }

    private static String getDateOfPrice(String line, int positionOfFourthDelimiter, int positionOfFifthDelimiter){
        return line.substring(positionOfFourthDelimiter,positionOfFifthDelimiter);
    }

}

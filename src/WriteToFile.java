import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {

    public static final String fileName = "file.txt";

    public static void WriteToFile () {

        File file = new File(fileName);

        Provider provider1 = new Provider("|name1|","London|","2|","13.02|");
        Provider provider2 = new Provider("|name2|","New York|","2|","12.03|");
        Provider provider3 = new Provider("|name3|","Chernivtsi|","1|","11.03|");

        try {

            FileWriter fr = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(fr);

            bufferedWriter.write(String.valueOf(provider1));
            bufferedWriter.newLine();
            bufferedWriter.write(String.valueOf(provider2));
            bufferedWriter.newLine();
            bufferedWriter.write(String.valueOf(provider3));
            bufferedWriter.flush();
            bufferedWriter.close();

        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
